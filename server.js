/* eslint-disable no-unused-vars */
const path = require('path');
const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

app.get('/', (req, res) => {
    res.sendFile(path.join(publicPath, 'index.html'));
});

const publicPath = path.join(__dirname, '..', './build/');
app.use(express.static(publicPath));

app.listen(port, (req, res) => {
    console.log(`server listening on port: ${port}`);
});